<!DOCTYPE html>
<html lang="pt-br">
  <head>
    <!-- Meta tags Obrigatórias -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="public_html/css/bootstrap.min.css">   
    <!-- custom CSS -->
    <link rel="stylesheet" href="public_html/css/estilo.css">
    <link rel="stylesheet" href="">    

    <title>Base de conhecimento</title>
    <style>
        div.realocar{margin: 0 250px;}       /* Correção da centralização das imagens */
        p.text-justify{text-indent: 2em;}    /* Paragráfo */
        div.subtitle{color: #FFFFFF;}
        li.dropdown{margin-right: 5px;}     /* Espaçamento entre <ul> <li> */
        li.nav-item:last-child{margin-left: 20px;}   /* Espaçamento somente na última <li> do navbar*/
    </style>

  </head>
  <body>

    <!-- Barra de navegação MENU -->   
<nav class="navbar navbar-expand-sm fixed-top navbar-dark bg-dark">
    <div class="container">
      <img class="brasao" src="arquivos/brasao.jpg" />&nbsp;&nbsp;&nbsp;
        <h1><a class="navbar-brand" href="#">DRPS</a></h1>
        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#conteudoNavbarSuportado" aria-controls="conteudoNavbarSuportado" aria-expanded="false" aria-label="Alterna navegação">
            <span class="navbar-toggler-icon"></span>
        </button>

        <div class="collapse navbar-collapse" id="conteudoNavbarSuportado">
            <ul class="navbar-nav ml-auto">
                <!-- <li class="nav-item active">
                    <a class="nav-link" href="#">Home <span class="sr-only">(página atual)</span></a>
                </li> -->
                <li class="nav-item">
                    <a class="nav-link" href="#">Notícias</a>
                </li>              
                <li class="nav-item dropdown">
                    <a class="nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                    Departamento 
                    </a>
                    <div class="dropdown-menu" aria-labelledby="navbarDropdown">
                      <a class="dropdown-item" href="#">SEAS</a>
                      <a class="dropdown-item" href="#">DRPS</a>
                      <a class="dropdown-item" href="#">DROIS</a>
                    <div class="dropdown-divider"></div>
                      <a class="dropdown-item" href="#">DRPS - ROTINAS E PROCEDIMENTOS</a>
                    </div>
                </li>     
                <li class="nav-item dropdown">
                    <a class="nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                    Base de Conhecimento
                    </a>
                    <div class="dropdown-menu" aria-labelledby="navbarDropdown">   
                      <a class="dropdown-item" href="#">Sistemas</a>                     
                      <a class="dropdown-item" href="#">Infraestrutura</a>
                      <a class="dropdown-item" href="#">Desenvolvimento</a>                        
                    <div class="dropdown-divider"></div>
                      <a class="dropdown-item" href="#">Ambiente de Testes</a>
                      <a class="dropdown-item" href="#">Ambiente de Produção</a>
                      <a class="dropdown-item" href="#">Ambiente de Homologação</a>
                      <a class="dropdown-item" href="#">Ambiente de Desenvolvimento</a>
                    </div>
                </li>  
                <li class="nav-item dropdown">
                      <a class="nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                      Ajuda
                      </a>
                    <div class="dropdown-menu" aria-labelledby="navbarDropdown">                        
                      <a class="dropdown-item" href="#">Contatos</a>  
                      <a class="dropdown-item" href="#">Presidência</a>                      
                      <a class="dropdown-item" href="#">Layout atual</a>
                    <div class="dropdown-divider"></div>                      
                      <a class="dropdown-item" href="#">Dica Rápida</a>
                    </div>
                </li>   
                <li class="nav-item">
                  <button type="button" class="btn btn-success my-2 my-sm-0" type="submit">Entrar</button>
                </li>             
            </ul>           
        </div>
  </div>
</nav>

 <!-- Como um link -->
 <nav class="navbar navbar-dark bg-dark">
  <a class="navbar-brand" href="#">PR</a>
</nav>

<!-- Como um span -->
<nav class="navbar navbar-dark bg-dark">
   <div class="container">
  <!-- <span class="navbar-brand mb-3 h1">LGPD</span>  -->
    <div class="menu">
          <!-- <button class="br-button" type="button" circle mini><i class="fas fa-bars"></i>
          </button> -->
          <div class="title">
            <!-- <a class="" href="index.php">DRPS</a> -->
            <span class="navbar-brand mb-3 h1">O seu suporte on-line</span> 
          </div>
          <div class="subtitle">Desenvolvimento de Sistemas</div>
    </div>
        <div class="search">
            <form class="form-inline my-2 my-lg-0">
                <input class="form-control mr-sm-2" type="search" placeholder="Pesquisar" aria-label="Pesquisar">
                <button type="button" class="btn btn-outline-primary my-2 my-sm-0" type="submit">Pesquisar</button>
            </form>
        </div>
    </div> 
</nav>

<!-- Conteúdo do corpo da aplicação -->

<div class="card">
    <div class="container">    
        <!-- <div class="card-header">
            !!!!!texto!!!!!
        </div> -->
        <div class="card-body">
            <blockquote class="blockquote mb-0">
                <p>Não existe problema sem solução!</p>
                <footer class="blockquote">Romulo de Oliveira Azevedo <br/><cite title="Título da fonte">By Estagiário</cite></footer>
            </blockquote>
        </div>
    </div>
</div>

<div class="container-sm realocar mt-1"> 
    <div class="row">      
        <div class="column col-sm-4 col-sx4">
            <img src="arquivos/manutenção1.jpg" alt="..." class="rounded-circle">       
        </div>
    
    
        <div class="column col-sm-4 col-xs-4">
            <img src="arquivos/manutenção3.jpg" alt="..." class="rounded-circle">     
        </div>
    
   
        <div class="column col-sm-4 col-xs-4">
            <img src="arquivos/manutenção2.jpg" alt="..." class="rounded-circle" style="height: 180px;">        
        </div>      
    </div>
</div>

<div class="container">
    <div class="row mt-2">
        <div class="col-sm-6">
            <div class="card">
            <div class="card-body">
                <h5 class="card-title">Biblioteca Virtual</h5>
                <p class="card-text text-justify">A Biblioteca virtual oferece um conjunto de serviços digitais, possibilitando o estudo e pesquisa por meio da internet.</p>
                <a href="#" class="btn btn-primary">Visitar</a>
            </div>
            </div>
        </div>
        <div class="col-sm-6">
            <div class="card">
            <div class="card-body">
                <h5 class="card-title">Guia de Integração, Profissionais DRPS/SEAS</h5>
                <p class="card-text text-justify">Política: Todos os servidores devem comprometer-se a respeitar os princípios estabelecidos por meio da política do DRPS.</p>
                <a href="#" class="btn btn-primary">Visitar</a>
            </div>
            </div>
        </div>
    </div>
</div>

<div class="container mt-2">
    <div class="accordion" id="accordionExample">
        <div class="card">
            <div class="card-header" id="headingOne">
            <h5 class="mb-0">
                <button class="btn btn-link" type="button" data-toggle="collapse" data-target="#collapseOne" aria-expanded="false" aria-controls="collapseOne">
                Endomarketing
                </button>
            </h5>
            </div>

            <div id="collapseOne" class="collapse show" aria-labelledby="headingOne" data-parent="#accordionExample">
            <div class="card-body">
                <p class="text-justify">É uma estratégia de marketing institucional voltada para ações internas na empresa. É também chamado de Marketing Interno e visa melhorar a imagem da empresa entre os seus colaboradores, culminando em uma equipe motivada e reduzindo o turnover.: "É a taxa de rotatividade de funcionários, que mede o número de funcionários que saem de uma organização durante um período de tempo especificado (normalmente um ano)".</p>
            </div>
            </div>
        </div>
        <div class="card">
            <div class="card-header" id="headingTwo">
            <h5 class="mb-0">
                <button class="btn btn-link collapsed" type="button" data-toggle="collapse" data-target="#collapseTwo" aria-expanded="false" aria-controls="collapseTwo">
                Desenvolvimento de Sistemas
                </button>
            </h5>
            </div>
            <div id="collapseTwo" class="collapse" aria-labelledby="headingTwo" data-parent="#accordionExample">
            <div class="card-body">
                <p class="text-justify">É o segmento da computação que, utilizando das linguagens de programação, cria e implementa as soluções de informática.</p>
            </div>
            </div>
        </div>
        <div class="card">
            <div class="card-header" id="headingThree">
            <h5 class="mb-0">
                <button class="btn btn-link collapsed" type="button" data-toggle="collapse" data-target="#collapseThree" aria-expanded="false" aria-controls="collapseThree">
                Infraestrutura
                </button>
            </h5>
            </div>
            <div id="collapseThree" class="collapse" aria-labelledby="headingThree" data-parent="#accordionExample">
            <div class="card-body">
            <p class="text-justify">Consiste nos componentes e serviços que fornecem a base para sustentar todos os sistemas de informação de uma organização.</p>
            </div>
            </div>
        </div>
        <div class="card">
            <div class="card-header" id="headingfour">
            <h5 class="mb-0">
                <button class="btn btn-link collapsed" type="button" data-toggle="collapse" data-target="#collapsefour" aria-expanded="false" aria-controls="collapsefour">
                Programação
                </button>
            </h5>
            </div>
            <div id="collapsefour" class="collapse" aria-labelledby="headingfour" data-parent="#accordionExample">
            <div class="card-body">
               <p class="text-justify">É um processo de escrita, testes e manutenção de programas de computadores. Esses programas, por sua vez, são compostos por conjuntos de instruções determinados pelo programador que descrevem tarefas a serem realizadas pela máquina e atendem diversas finalidades.</p>
            </div>
            </div>
        </div>
    </div>
</div>

<div class="container">
    <div class="embed-responsive embed-responsive-21by9">
    <!-- <iframe class="embed-responsive-item" src="..."></iframe> -->
    <iframe width="560" height="315" src="https://www.youtube.com/embed/6Af6b_wyiwI" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
    </div>
</div>

<!-- Footer -->
<footer class="bg-dark text-center text-white">
  <!-- Grid container -->
  <div class="container p-4">
    <!-- Section: Social media -->
 
    <!-- Section: Social media -->

    <!-- Section: Form -->
    <section class="">
      <form action="">
        <!--Grid row-->
        <div class="row d-flex justify-content-center">
          <!--Grid column-->
          <div class="col-auto">
            <p class="pt-2">
              <strong>Venha fazer parte da nossa equipe!</strong>
            </p>
          </div>
          <!--Grid column-->

          <!--Grid column-->
          <div class="col-md-5 col-12">
            <!-- Email input -->
            <div class="form-outline form-white mb-4">
              <input type="email" id="form5Example2" class="form-control" />
              <label class="form-label" for="form5Example2">Endereço de E-mail</label>
            </div>
          </div>
          <!--Grid column-->

          <!--Grid column-->
          <div class="col-auto">
            <!-- Submit button -->
            <button type="submit" class="btn btn-outline-light mb-4">
              Inscreva-se
            </button>
          </div>
          <!--Grid column-->
        </div>
        <!--Grid row-->
      </form>
    </section>
    <!-- Section: Form -->

    <!-- Section: Text -->
    <section class="mb-4">
      <p>
      Os benefícios de usar uma base de conhecimento no seu processo de suporte ao colaborador.
      </p>
    </section>
    <!-- Section: Text -->

    <!-- Section: Links -->
    <section class="">
      <!--Grid row-->
      <div class="row">
        <!--Grid column-->
        <div class="col-lg-3 col-md-6 mb-4 mb-md-0">
          <h5 class="text-uppercase">Links</h5>
          <ul class="list-unstyled mb-0">
            <li>
              <a href="#!" class="text-white">Link 1</a>
            </li>
            <li>
              <a href="#!" class="text-white">Link 2</a>
            </li>
            <li>
              <a href="#!" class="text-white">Link 3</a>
            </li>
            <li>
              <a href="#!" class="text-white">Link 4</a>
            </li>
          </ul>
        </div>
        <!--Grid column-->

        <!--Grid column-->
        <div class="col-lg-3 col-md-6 mb-4 mb-md-0">
          <h5 class="text-uppercase">Links</h5>

          <ul class="list-unstyled mb-0">
            <li>
              <a href="#!" class="text-white">Link 1</a>
            </li>
            <li>
              <a href="#!" class="text-white">Link 2</a>
            </li>
            <li>
              <a href="#!" class="text-white">Link 3</a>
            </li>
            <li>
              <a href="#!" class="text-white">Link 4</a>
            </li>
          </ul>
        </div>
        <!--Grid column-->

        <!--Grid column-->
        <div class="col-lg-3 col-md-6 mb-4 mb-md-0">
          <h5 class="text-uppercase">Links</h5>

          <ul class="list-unstyled mb-0">
            <li>
              <a href="#!" class="text-white">Link 1</a>
            </li>
            <li>
              <a href="#!" class="text-white">Link 2</a>
            </li>
            <li>
              <a href="#!" class="text-white">Link 3</a>
            </li>
            <li>
              <a href="#!" class="text-white">Link 4</a>
            </li>
          </ul>
        </div>
        <!--Grid column-->

        <!--Grid column-->
        <div class="col-lg-3 col-md-6 mb-4 mb-md-0">
          <h5 class="text-uppercase">Links</h5>

          <ul class="list-unstyled mb-0">
            <li>
              <a href="#!" class="text-white">Link 1</a>
            </li>
            <li>
              <a href="#!" class="text-white">Link 2</a>
            </li>
            <li>
              <a href="#!" class="text-white">Link 3</a>
            </li>
            <li>
              <a href="#!" class="text-white">Link 4</a>
            </li>
          </ul>
        </div>
        <!--Grid column-->
      </div>
      <!--Grid row-->
    </section>
    <!-- Section: Links -->
  </div>
  <!-- Grid container -->

  <!-- Copyright -->
  <div class="text-center p-3" style="background-color: rgba(0, 0, 0, 0.2);">
    © 2021 Copyright:
    <a class="text-white" href="">Base de conhecimento.com</a>
  </div>
  <!-- Copyright -->
</footer>
<!-- Footer -->


    <script src="public_html/js/jquery-3.3.1.slim.min.js"></script>
    <script src="public_html/js/popper.min.js"></script>
    <script src="public_html/js/bootstrap.min.js"></script>
    
  </body>
</html>